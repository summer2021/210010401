# Summer2021-No.73 为iSulad的查询（inspec）命令增加size以及type子命令

#### Description

https://gitee.com/openeuler-competition/summer-2021/issues/I3EPVK

#### Installation

1.  https://gitee.com/openeuler/iSulad/blob/master/docs/install_iSulad_on_Ubuntu_20_04_LTS.sh
2.  https://gitee.com/openeuler/lcr

#### Instructions

https://gitee.com/openeuler/iSulad#https://openeuler.org/zh/docs/20.09/docs/Container/iSula%E5%AE%B9%E5%99%A8%E5%BC%95%E6%93%8E.html

#### Contribution

1.  Fork the repository
2.  Create Feat_xxx branch
3.  Commit your code
4.  Create Pull Request
