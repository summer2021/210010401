# Summer2021-No.73 为iSulad的查询（inspec）命令增加size以及type子命令

#### 介绍

https://gitee.com/openeuler-competition/summer-2021/issues/I3EPVK

#### 分支说明
+ master：项目相关说明及使用
+ iSulad：iSulad --size及--type实现
+ iSulad-fix：新功能或者补丁
+ lcr：lcr相关修改

#### 正式版本
https://gitee.com/xiyounigo/iSulad

#### 安装教程

1.  https://gitee.com/openeuler/iSulad/blob/master/docs/install_iSulad_on_Ubuntu_20_04_LTS.sh

2.  https://gitee.com/openeuler/lcr

#### 使用说明

https://gitee.com/openeuler/iSulad#https://openeuler.org/zh/docs/20.09/docs/Container/iSula%E5%AE%B9%E5%99%A8%E5%BC%95%E6%93%8E.html

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request
